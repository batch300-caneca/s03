<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03 Activity Classes and Objects</title>
</head>
<body>
    <h1>Person</h1>
    <p><?= $human1->printName(); ?></p>

    <h1>Developer</h1>
    <p><?= $human2->printName(); ?></p>

    <h1>Engineer</h1>
    <p><?= $human3->printName(); ?></p>
</body>
</html>