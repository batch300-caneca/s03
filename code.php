<?php

class Person {
    public $firstName;
    public $lastName;

    public function __construct($firstName, $lastName){
        $this->firstName =  $firstName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your full name is $this->firstName $this->lastName.";
    }
};

class Developer extends Person{
    public $middleName;
    public $profession;

    public function __construct($firstName, $middleName, $lastName){
        parent::__construct($firstName, $lastName);
        $this->middleName = $middleName;
    }

    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }
};

class Engineer extends Developer{
    public function printName(){
        return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
};

$human1 = new Person("Senku", "Ishigami");
$human2 = new Developer("John", "Finch", "Smith");
$human3 = new Engineer("Harold", "Myers", "Reese");